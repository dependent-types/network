-- | An example module.
module Example ( main
               , withLayer
               , Layer
               , layer
               ) where

import GHC.TypeLits
import Data.Proxy
import Data.Function ((&))

type DownL2 = Layer "l2" 2 1
type DownNet = Layer "net" 3 2
type DownTcp = Layer "tcp" 4 3
type DownHttp = Layer "http" 5 4

type UpL2 = Layer "l2" 1 2
type UpNet = Layer "net" 2 3
type UpTcp = Layer "tcp" 3 4
type UpHttp = Layer "http" 4 5

data Layer (lbl :: Symbol) (m :: Nat) (n :: Nat) where
  Layer :: (KnownSymbol lbl, KnownNat m, KnownNat n) => Layer lbl m n
  (:>->:) :: Layer lbl1 m1 n1 -> Layer lbl2 n1 n2 -> Layer (AppendSymbol (AppendSymbol lbl1 ":->:") lbl2) m1 n2

instance Show (Layer lbl m n) where
  showsPrec _ Layer = \_ -> ""
                        <> (symbolVal (undefined :: Proxy lbl))
                        <> "("
                        <> (show $ natVal (undefined :: Proxy m))
                        <> ","
                        <> (show $ natVal (undefined :: Proxy n))
                        <> ")"
  showsPrec _ (m1 :>->: m2) = \_ -> "(" <> show m1 <> ":>->:" <> show m2 <> ")"

(~>) = (:>->:)

withLayer :: forall r a b . (Integral a, Integral b) => String -> a -> b -> (forall (lbl :: Symbol) (m :: Nat) (n :: Nat) . Layer lbl m n -> IO r) -> IO r
withLayer (someSymbolVal -> SomeSymbol (_ :: Proxy lbl)) (someNatVal . fromIntegral -> Just (SomeNat (_ :: Proxy m))) (someNatVal . fromIntegral -> Just (SomeNat (_ :: Proxy n))) fn = fn (Layer :: Layer lbl m n)

layer :: forall a b m n lbl . (Integral a, Integral b, KnownNat m, KnownNat n, KnownSymbol lbl) => String -> a -> b -> (Maybe (Layer lbl m n))
layer  ((symbolVal (undefined :: Proxy lbl) ==) -> True ) ((natVal (undefined :: Proxy m) ==) . fromIntegral -> True) ((natVal (undefined :: Proxy n) ==) . fromIntegral -> True)  = return Layer
layer _ _ _ = Nothing

-- | An example function.
main :: IO ()
main = do
  withLayer "http" 3 4 (print)
  withLayer "net" 4 5 (print)

  (layer "test" 3 4 :: (Maybe (Layer "test" 3 4))) & print
  (layer "test" 5 4 :: (Maybe (Layer "test" 5 4))) & print

  let
    l2Down :: (Maybe DownL2)
    l2Down = layer "l2" 2 1

    l2Up :: (Maybe UpL2)
    l2Up = layer "l2" 1 2

    netDown :: (Maybe DownNet)
    netDown = layer "net" 3 2

    netUp :: (Maybe UpNet)
    netUp = layer "net" 2 3

    tcpDown :: (Maybe DownTcp)
    tcpDown = layer "tcp" 4 3

    tcpUp :: (Maybe UpTcp)
    tcpUp = layer "tcp" 3 4

    httpDown :: (Maybe DownHttp)
    httpDown = layer "http" 5 4

    httpUp :: (Maybe UpHttp)
    httpUp = layer "http" 4 5

  let
    system :: IO ()
    system = print do
      httpDown >>= \httpD -> do
        tcpDown >>= \tcpD -> do
          netDown >>= \netD -> do
            l2Down >>= \l2D -> do
              l2Up >>= \l2U -> do
                netUp >>= \netU -> do
                  tcpUp >>= \tcpU -> do
                    httpUp >>= \httpU -> do
                      return $
                        httpD
                        ~> tcpD
                        ~> netD
                        ~> netU
                        ~> netD
                        ~> l2D
                        ~> l2U
                        ~> netU
                        ~> tcpU
                        ~> httpU
  system
